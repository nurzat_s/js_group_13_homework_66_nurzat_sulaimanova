export class Country {
  constructor(
    public id: string,
    public name: string,
    public capital: string,
    public population: string,
    public alpha3Code: string,
    public region: string
  ) {}
}
