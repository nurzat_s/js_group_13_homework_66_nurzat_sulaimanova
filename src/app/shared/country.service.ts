import { Injectable } from '@angular/core';
import { map, Subject } from 'rxjs';
import { Country } from './country.model';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CountryService {
  countriesChange = new Subject<Country[]>();
  countriesFetching = new Subject<boolean>();
  countryUploading = new Subject<boolean>();

  constructor(private http: HttpClient) {}

  private countries: Country[] = [];

  fetchCountries() {
    this.countriesFetching.next(true);
    this.http.get<{[id: string]: Country}>('http://146.185.154.90:8080/restcountries/rest/v2/all?fields=name;alpha3Code.json')
      .pipe(map(result => {
        console.log(result)
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(alpha3Code => {
          const countryData = result[alpha3Code];
          return new Country(alpha3Code, countryData.name, countryData.capital, countryData.population, countryData.alpha3Code, countryData.region);
        });

      }))
      .subscribe(countries => {
        this.countries = countries;
        this.countriesChange.next(this.countries.slice());
        this.countriesFetching.next(false);
      }, error => {
        this.countriesFetching.next(false);
      })
  }

  getCountries() {
    return this.countries.slice();
  }

  fetchCountry(code: string) {
    return this.http.get<Country>(`http://146.185.154.90:8080/restcountries/rest/v2/alpha/${code}`).pipe(
      map(result => {
        console.log(result)
        return new Country(code, result.name, result.capital, result.population, result.alpha3Code, result.region);
      })
    );
  }



}
