import { Component, Input, OnInit } from '@angular/core';
import { Country } from '../../shared/country.model';

@Component({
  selector: 'app-countries-item',
  templateUrl: './countries-item.component.html',
  styleUrls: ['./countries-item.component.css']
})
export class CountriesItemComponent implements OnInit {
  @Input() country!: Country;

  constructor() { }

  ngOnInit(): void {
  }

}
