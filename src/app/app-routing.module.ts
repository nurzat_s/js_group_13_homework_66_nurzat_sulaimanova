import { RouterModule, Routes } from '@angular/router';
import { CountriesComponent } from './countries/countries.component';
import { CountryDetailsComponent } from './country-details/country-details.component';
import { CountryResolverService } from './country-resolver.service';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {path: '', component: CountriesComponent},
      {
        path: ':alpha3Code',
        component: CountryDetailsComponent,
        resolve: {
          alpha3Code: CountryResolverService
        }
      },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
