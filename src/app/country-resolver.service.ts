import { Injectable } from '@angular/core';
import { Country } from './shared/country.model';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { CountryService } from './shared/country.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CountryResolverService implements Resolve<Country>{

  constructor(private countryService: CountryService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Country> {
    const countryCode = <string>route.params['alpha3Code'];
    console.log(countryCode)
    return this.countryService.fetchCountry(countryCode);

  }


}
